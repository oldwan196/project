<?php
    require_once("DButil.php");
    $course1 = $_REQUEST["course1"];
    $coursename1 = $_REQUEST["coursename1"];
    
    
    $sql = " SELECT ".
            "t_video.videoId, ".
            "t_course.courseName, ".
            "t_video.videoInfo, ".
            "t_video.courseId, ".
            "t_video.teacherId, ".
            "t_video.`status`, ".
            "t_teacher.teacherId, ".
            "t_teacher.teacherName ".
        "FROM t_course ,t_video ,t_teacher ".
    "WHERE ".
        "t_course.courseId = t_video.courseId AND ".
        "t_video.teacherId = t_teacher.teacherId ".
    "GROUP BY ".
        "t_video.videoId; ";

    $teacher = query($sql);

    $result = [
        "msg" => "查询成功",
        "code" => 200,
        "success" => true,
        "data" => $teacher
    ];

    header("Content-Type:application/json;charset=utf-8");
    echo json_encode($result);

?>