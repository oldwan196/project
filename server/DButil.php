<?php
/**
 * 获取连接对象
 */
function getConn() {
    $url = "localhost:3306";
    $user = "root";
    $password = "";// 如果没有密码, 就是空字符串
    $database = "videoDatas";
    $conn = mysqli_connect($url, $user, $password, $database);
    mysqli_query($conn, "set names utf8");
    return $conn;
}

/**
 * 增删改操作
 */
function update($sql) {
    $conn = getConn();
    $result = mysqli_query($conn, $sql);
    mysqli_close($conn);
    return $result;
}

/**
 * 增加并返回主键值
 */
function insertReturnKey($conn, $sql) {
    $result = mysqli_query($conn, $sql);
    if($result){
        $id = mysqli_insert_id($conn);
        return $id;
    }
    return false;
}

/**
 * 查询操作
 */
function query($sql) {
    $conn = getConn();
    $result = mysqli_query($conn, $sql);
    $list = [];
    while($row = mysqli_fetch_assoc($result)){
        $list[] = $row;
    }
    mysqli_close($conn);
    return $list;
}

function queryOne($sql) {
    $conn = getConn();
    $result = mysqli_query($conn, $sql);
    $one = null;
    if($row = mysqli_fetch_assoc($result)){
        $one = $row;
    }
    mysqli_close($conn);
    return $one;
}
?>