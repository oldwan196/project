drop database if exists videoDatas;

create database videoDatas default charset=utf8;

use videoDatas;

create table t_role(
    roleId int primary key auto_increment,
    roleName varchar(100)
)engine=Innodb default charset=utf8 AUTO_INCREMENT=1001;

create table t_power(
    powerId int primary key auto_increment,
    powerName varchar(100),
    powerPath varchar(200)
)engine=Innodb default charset=utf8 AUTO_INCREMENT=2001;

create table t_role_power(
    rolePowerId int primary key auto_increment,
    roleId int,
    powerId int,
    constraint role_power_role_fk foreign key(roleId) references t_role(roleId),
    constraint role_power_power_fk foreign key(powerId) references t_power(powerId)
)engine=Innodb default charset=utf8 AUTO_INCREMENT=3001;

create table t_teacher(
    teacherId int primary key auto_increment,
    teacherName varchar(100),
    username varchar(100),
    password varchar(100),
    status int comment "0:禁用,1:启用",
    roleId int,
    constraint teacher_role_fk foreign key(roleId) references t_role(roleId)
)engine=Innodb default charset=utf8 AUTO_INCREMENT=4001;

create table t_course(
    courseId int primary key auto_increment,
    courseName varchar(100),
    status int comment "0:禁用,1:启用"
)engine=Innodb default charset=utf8 AUTO_INCREMENT=5001;

create table t_video(
    videoId int primary key auto_increment,
    videoInfo varchar(600),
    courseId int,
    teacherId int,
    videoPath varchar(200),
    status int comment "0:待审核,1:审核未通过,2:审核通过,3:视频上架,4、视频下架",
    constraint video_teacher_fk foreign key(teacherId) references t_teacher(teacherId),
    constraint video_course_fk foreign key(courseId) references t_course(courseId)
)engine=Innodb default charset=utf8 AUTO_INCREMENT=6001;