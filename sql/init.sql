insert into t_role values (1001,"超级管理员"),
                          (1002,"普通管理员"),
                          (1003,"老师");

insert into t_power values (2001,"视频统计","statisticsPage.html"),
                           (2002,"权限管理","rolePage.html"),
                           (2003,"课程管理","coursePage.html"),
                           (2004,"老师管理","teacherPage.html"),
                           (2005,"视频管理","vedioPage.html"),
                           (2006,"我的视频","myVedioPage.html");


insert into t_role_power values (null,1001,2001),
                                (null,1001,2002),
                                (null,1001,2003),
                                (null,1001,2004),
                                (null,1001,2005),
                                (null,1002,2001),
                                (null,1002,2003),
                                (null,1002,2004),
                                (null,1002,2005),
                                (null,1002,2006),
                                (null,1003,2001),
                                (null,1003,2006);

insert into t_teacher values (null,"张三","zhangsan","000000",1,1001),
                             (null,"李四","lisi","000000",0,1001),
                             (null,"王二","wanger","000000",1,1002),
                             (null,"麻子","mazi","000000",1,1003);
