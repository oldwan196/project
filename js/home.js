var tabsA = $("#tabsA");
var tabsB = $("#tabsB");

function changePage(tab){
    $(".tab-menus .active").removeClass("active");
    $.each(tab.doms,(i,v)=>void v.addClass("active"));
    $("#pageContent").load(tab.path+"?_="+new Date().getTime());
}
function initTabs(data){
    for (const tab of data) {
        let domA = $(`
            <li class="nav-item">
                <a class="nav-link">${tab.name}</a>
            </li>
        `);
        let domB = $(`<a class="list-group-item">${tab.name}</a>`);
        tab.doms = [];
        tab.doms.push(domA);
        tab.doms.push(domB);

        domA.on("click",changePage.bind(this,tab));
        domB.on("click",changePage.bind(this,tab));
        
        tabsA.append(domA);
        tabsB.append(domB)
    }
    changePage(data[0]);
}

$(function(){
    // var username = "zhangsan";
    $.ajax({
        type:"get",
        url:"./server/home.php",
        data:{},
        success:function(res){
            // console.log(res);
            if(res.roleId=="1001"){
                initTabs([
                    {
                        name:"视频统计",
                        path:"./template/statisticsPage.html"
                    },
                    {
                        name:"权限管理",
                        path:"./template/rolePage.html"
                    },
                    {
                        name:"课程管理",
                        path:"./template/coursePage.html"
                    },
                    {
                        name:"老师管理",
                        path:"./template/teacherPage.html"
                    },
                    {
                        name:"视频管理",
                        path:"./template/vedioPage.html"
                    }
                ]);
            }if(res.roleId=="1002"){
                initTabs([
                    {
                        name:"视频统计",
                        path:"./template/statisticsPage.html"
                    },
                    {
                        name:"课程管理",
                        path:"./template/coursePage.html"
                    },
                    {
                        name:"老师管理",
                        path:"./template/teacherPage.html"
                    },
                    {
                        name:"视频管理",
                        path:"./template/vedioPage.html"
                    },
                    {
                        name:"我的视频",
                        path:"./template/myVedioPage.html"
                    }
                ]);
            }if(res.roleId=="1003"){
                initTabs([
                    {
                        name:"视频统计",
                        path:"./template/statisticsPage.html"
                    },
                    {
                        name:"我的视频",
                        path:"./template/myVedioPage.html"
                    }
                ]);
            }
            $(".username").text(res.username)
        },
        error:function(e,q,w){
            console.log(w);
        }
    })
    
})


function loginOut(params) {
    console.log(123);

    location.href = "./index.html";

}






